//
//  LoadingView.swift
//  Weather
//
//  Created by Antoine Carrel on 19/06/2022.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        VStack(spacing: 20) {
            HStack {
                VStack(spacing: 20) {
                    Image("logo")
                    Text("Recherche de votre position 👀")
                        .font(.title.bold())
                }
            }
            .padding(.top, 100)
            
            Spacer()
            
            ProgressView(value: 0.75)
                .progressViewStyle(CircularProgressViewStyle(tint: .orange))
                .scaleEffect(4)
                .frame(maxWidth: 50, maxHeight: 50)
        }
        .padding(.bottom, 100)
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
