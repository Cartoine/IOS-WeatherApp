//
//  WelcomeView.swift
//  Weather
//
//  Created by Antoine Carrel on 19/06/2022.
//

import SwiftUI
import CoreLocationUI

struct WelcomeView: View {
    @EnvironmentObject var locationManager: LocationManager
    
    var body: some View {
        VStack {
            
            VStack(spacing: 20) {
                HStack {
                    VStack(spacing: 20) {
                        Image("logo")
                        Text("Météo PRO pour les pros")
                            .font(.title.bold())
                    }
                }
                .frame(width: 400, alignment: .center )
                
                HStack {
                    VStack(spacing: 1) {
                        Image("weatherLogo")
                            .resizable()
                            .frame(width: 300, height: 300)
                    }
                }
                .frame(width: 200, alignment: .center )
                
                Text("Partage ta position pour obtenir la météo ou regarde le ciel")
                    .padding()
                    .font(.system(size: 24))
            }
            .multilineTextAlignment(.center)
            .padding()
            .foregroundColor(.white)
            
            LocationButton(.shareMyCurrentLocation) {
                locationManager.requestLocation()
            }
            
            .cornerRadius(30)
            .symbolVariant(.fill)
            .foregroundColor(.white)
            
            Spacer()
            
            VStack(spacing: 20) {
                Text("© cartoine")
                    .bold().font(.title)
                    .foregroundColor(.orange)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(.black)
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
