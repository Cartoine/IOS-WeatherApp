//
//  WeatherView.swift
//  Weather
//
//  Created by Antoine Carrel on 19/06/2022.
//

import SwiftUI
import CoreLocationUI
import Neumorphic

struct WeatherView: View {
    var weather: ResponseBody
    var mainColor = NeumorphicKit.colorType(red: 0, green: 0, blue: 0)
    
    @EnvironmentObject var locationManager: LocationManager
    var body: some View {
        ZStack(alignment: .leading) {
            VStack {
                VStack(alignment: .leading, spacing: 2) {
                    Text(weather.name)
                        .bold().font(.title)
                        .foregroundColor(.orange)
                    Text("Aujourd'hui \(Date().formatted(.dateTime.day().month().hour().minute()))")
                        .fontWeight(.light)
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                
                Spacer()
                
                VStack {
                    HStack {
                        VStack(spacing: 1) {
                            AsyncImage(url: URL(string: "https://openweathermap.org/img/wn/\(weather.weather[0].icon)@4x.png"))
                            { image in
                                image
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 150)
                            } placeholder: {
                                ProgressView()
                            }
                            .font(.system(size: 50))
                            Text(weather.weather[0].main)
                        }
                        .frame(width: 150, alignment: .leading)
                        
                        Spacer()
                        
                        Text(weather.main.feelsLike.roundDouble() + "°C")
                            .font(.system(size: 80))
                            .fontWeight(.bold)
                            .padding()
                            .foregroundColor(.orange)
                    }
                    .frame(height: 80)
                    
                    VStack {
                        
                        Spacer()

                        VStack {
                            HStack() {
                                WeatherMap()
                            }
                            .cornerRadius(20, corners: [.topLeft, .topRight, .bottomLeft, .bottomRight])
                        }
                        
                        Spacer()
                    }
                    .frame(maxWidth: .infinity)
                }
                .padding()
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                
                ScrollView {
                    VStack(alignment: .leading, spacing: 20) {
                        Text("Temps actuel")
                            .bold().padding(.bottom)
                        HStack {
                            WeatherRow(logo: "thermometer", name: "Min temp", value: (weather.main.tempMin.roundDouble() + "°"))
                           
                            Spacer()
                            
                            WeatherRow(logo: "thermometer", name: "Max temp", value: (weather.main.tempMax.roundDouble() + "°"))
                        }
                        HStack {
                            WeatherRow(logo: "wind", name: "Vent", value: (weather.wind.speed.roundDouble() + "m/s"))
                           
                            Spacer()
                            
                            WeatherRow(logo: "humidity", name: "Humidité", value: (weather.main.humidity.roundDouble() + "%"))
                        }
                        HStack {
                            WeatherRow(logo: "barometer", name: "Pression", value: (weather.main.pressure.roundDouble() + "hPa"))
                            
                            Spacer()
                            
                            HStack(spacing: 10) {
                                Image("compass")
                                    .resizable()
                                    .frame(width: 45, height: 45)
                                    .padding(5)
                                    .background(Color(hue: 1.0, saturation: 0.0, brightness: 0.888))
                                    .cornerRadius(50)
                                
                                VStack(alignment: .leading, spacing: 8) {
                                    Text("Direction \n du vent")
                                        .font(.caption)
                                    Text("\(weather.wind.deg.roundDouble() + "°")")
                                        .bold()
                                        .font(.title)
                                }
                                .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10))
                            }
                        }
                        HStack {
                            WeatherRow(logo: "cloud.sun.rain.fill", name: "Couverture \n nuageuse", value: (weather.clouds.all.roundDouble() + "%"))
                            
                            Spacer()
                            
                            AsyncImage(url: URL(string: "https://openweathermap.org/img/wn/\(weather.weather[0].icon)@4x.png"))
                            { image in
                                image
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 100)
                            } placeholder: {
                                ProgressView()
                            }
                            .font(.system(size: 50))
                        }
                        VStack {
                            Text("Les donnée météo provienne de l'API \n d'Openweather map")
                            Image("logoOpenWeatherMap")
                                .resizable()
                                .frame(width: 120, height: 54)
                        }
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                        .font(.system(size: 14).bold())
                        .multilineTextAlignment(.center)
                        
                        HStack {
                            Button(action: {}) {
                                Link("cartoine Lab", destination: URL(string: "https://gitlab.com/Cartoine")!)
                                    .font(.system(size: 20))
                                Image("gitlab")
                                    .resizable()
                                    .frame(width: 45, height: 45)
                                    .background(Color(hue: 1.0, saturation: 0.0, brightness: 0.888))
                                    .cornerRadius(50)
                            }
                            .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 0))
                            .background(.black)
                            .foregroundColor(.orange)
                            .clipShape(Capsule())
                            
                            Spacer()
                            
                            Button(action: {}) {
                                Link("My website", destination: URL(string: "https://antoinecarrel.com")!)
                                    .font(.system(size: 20))
                                Image("cartoine-logo")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 45, height: 45)
                                    .background(Color(hue: 1.0, saturation: 0.0, brightness: 0.888))
                                    .cornerRadius(50)
                            }
                            .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 0))
                            .background(.black)
                            .foregroundColor(.orange)
                            .clipShape(Capsule())
                        }
                    }
                    .padding()
                    .foregroundColor(Color.black)
                }
                .background(.orange)
                .cornerRadius(20, corners: [.topLeft, .topRight])
            }
            .edgesIgnoringSafeArea(.bottom)
            .background(Color.black)
            .preferredColorScheme(.dark)
        }
    }
}

struct WeatherView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherView(weather: previewWeather)
    }
}
