//
//  WeatherMap.swift
//  Weather
//
//  Created by Antoine Carrel on 20/06/2022.
//

import SwiftUI
import MapKit

struct WeatherMap: View {
    @StateObject var locationManager = LocationManager()
    let location = LocationHelper.currentLocation
    
    var lat  = ""
    var long = ""
    mutating func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location:CLLocationCoordinate2D = manager.location!.coordinate
        lat = String(location.latitude)
        long = String(location.longitude)
    }
    
    @State private var region: MKCoordinateRegion = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: MapDefaults.latitude, longitude: MapDefaults.longitude),
        span: MKCoordinateSpan(latitudeDelta: MapDefaults.zoom, longitudeDelta: MapDefaults.zoom))
    
    private enum MapDefaults {
        static let latitude = 49.2027592
        static let longitude = -0.3980893
        static let zoom = 0.2
    }
    
    var body: some View {
        
        VStack {
            Text("Vous etes trés exatement ici : ")
                .padding(5)
            Text("latitude: \(location.latitude), longitude: \(location.longitude)")
                .font(.system(size: 10).bold())
            Map(coordinateRegion: $region,
                interactionModes: .all,
                showsUserLocation: true)
        }
        .foregroundColor(.black)
        .background(Color.orange)
    }
}

struct WeatherMap_Previews: PreviewProvider {
    static var previews: some View {
        WeatherMap()
    }
}

