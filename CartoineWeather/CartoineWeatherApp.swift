//
//  CartoineWeatherApp.swift
//  CartoineWeather
//
//  Created by Antoine Carrel on 24/06/2022.
//

import SwiftUI

@main
struct CartoineWeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
